import re
from pprint import pprint

#regex_subject_code = [A-Z]{3}[0-9]{4}
REGEX_SLOT = '([A-Z]{3}[0-9]{4})-(.*)-(.*)-(.*)'  # (1:CODE)-(2:TYPE)-(3:SLOT)-(4:VENUE)
WEEK_DAYS = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']

THEORY_START = ('08:00 	09:00 	10:00 	11:00 	12:00 	- 	14:00 	15:00 	16:00 	17:00 	18:00 	18:50 	19:00').split()
THEORY_END = ('08:50 	09:50 	10:50 	11:50 	12:50 	- 	14:50 	15:50 	16:50 	17:50 	18:50 	19:00 	19:50').split()

LAB_START = ('08:00 	08:46 	10:00 	10:46 	11:31 	12:16 	14:00 	14:46 	16:00 	16:46 	17:31 	18:16 	-').split()
LAB_END = ('08:45 	09:30 	10:45 	11:30 	12:15 	13:00 	14:45 	15:30 	16:45 	17:30 	18:15 	19:00 	-').split()

Day_slot_time = {}

with open('tt_blank.txt','r') as tt_blank:
    for line in tt_blank:
        #print(line)
        Slot_time = {}
        counter = 0
        words = line.split('\t')
        for word in words[1:]:
            #print(word)
            for slot in word.split(' / '):
                #print(slot)
                slot_time = []
                if(re.match('^[L][0-9]+',slot)):
                    slot_time.append(LAB_START[counter])
                    slot_time.append(LAB_END[counter])
                elif(re.match('^[T]?[A-Z]{1,2}[0-9]',slot)):
                    slot_time.append(THEORY_START[counter])
                    slot_time.append(THEORY_END[counter])
                #print(slot_time)
                Slot_time[slot.strip()] = slot_time
            counter+=1
        #print(Slot_time)
        Day_slot_time[words[0].strip()] = Slot_time

pprint(Day_slot_time)

Day_slot = {}

with open('tt.txt','r') as tt:
    for line in tt:
		#print(line)
		if line.startswith(tuple(WEEK_DAYS)):
			pass
		else:
			continue
		
		slots = []
		words = line.split('\t')
		
		for word in words:
			slots.extend(list(re.findall(r'([A-Z]{3}[0-9]{4}-.*-.*-.*) ',word)))
			
		Day_slot[words[0].strip()] = slots

pprint(Day_slot)

def free_time(Day_slot,Day_slot_time):
    pass


